import jwt
import datetime
from functools import wraps
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify, request
from marshmallow import Schema, fields, post_load
from werkzeug.security import generate_password_hash, check_password_hash


app = Flask(__name__)
app.config['SECRET_KEY'] = 'thisissecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://drew:dragonball@localhost/exam'
db = SQLAlchemy(app)

# TODO:
# Ideally, last_login should be implemented as another table like 'login_history'.
# Code should be split up into an organized package structure.

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    email = db.Column(db.String(50))
    password = db.Column(db.String(250))
    last_login = db.Column(db.DateTime)

    def __repr__(self):
        return '<User %s>' % self.name


class UserSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    email = fields.Str()
    password = fields.Str(load_only=True)
    last_login = fields.DateTime()

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)


# Run the following from python when need to reset.
# from main import db, User
# from werkzeug.security import generate_password_hash
# db.drop_all()
# db.create_all()
# new_user = User(name='Drew', email='andrew.delfin@gmail.com', password=generate_password_hash('test_123'))
# db.session.add(new_user)
# db.session.commit()


def check_for_token(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            token = None
            rh = request.headers
            if 'x-access-tokens' in rh:
                token = rh['x-access-tokens']
            if not token:
                return jsonify({'message': 'Token missing'}), 403
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms="HS256")
        except:
            return jsonify({'message': 'Invalid token'}), 403
        return f(*args, **kwargs)
    return decorated


@app.route('/login')
def login():
    auth = request.authorization
    if auth and auth.username and auth.password:
        user = User.query.filter_by(email=auth.username).first()
        if user and check_password_hash(user.password, auth.password):
            user.last_login = datetime.datetime.now()
            db.session.commit()
            data = {'user' : auth.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=20)}
            token = jwt.encode(data, app.config['SECRET_KEY'], algorithm="HS256")
            return jsonify({'token' : token})
    return jsonify({'message': 'Unable to verify'}), 403


@app.route("/user")
def get_users():
    schema = UserSchema(many=True)
    users = schema.dump(User.query.all())
    return jsonify(users)


@app.route("/user", methods=['POST'])
@check_for_token
def add_user():
    user = UserSchema().load(request.get_json())
    db.session.add(user)
    db.session.commit()
    return "", 204


@app.route("/user/<int:id>", methods=["PATCH"])
@check_for_token
def edit_user(id):
    user = User.query.filter_by(id=id).first()
    if user is None:
        return jsonify({'message': 'User not found'}), 404
    data = request.get_json()
    if 'name' in data:
        user.name = data['name']
    if 'email' in data:
        user.email = data['email']
    if 'password' in data:
        user.password = data['password']
    db.session.commit()
    return UserSchema().dump(user)


@app.route("/user/<int:id>", methods=['DELETE'])
@check_for_token
def delete_user(id):
    User.query.filter_by(id=id).delete()
    db.session.commit()
    return "", 204


